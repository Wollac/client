package powsrvio

import (
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"sync"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"

	"github.com/iotaledger/iota.go/trinary"

	"gitlab.com/powsrv.io/go/api"
)

const (
	serverName    string = "api.powsrv.io"
	serverAddress string = "api.powsrv.io:443"
)

// PendingPowRequest contains the information about a request and the response and error channel for signaling
type PendingPowRequest struct {
	powRequest      *api.PowRequest
	powResponseChan chan *api.PowResponse
	errorChan       chan error
}

// PowClient is the client that connects to the powsrv.io
type PowClient struct {
	ReadTimeOutMs          int64 // Timeout in ms to receive a response
	APIKey                 string
	Verbose                bool             // Print info messages
	connection             *grpc.ClientConn // Connection to the powsrv.io
	connectionError        error
	connectionErrorOcurred chan struct{}
	powStream              api.Pow_DoPowClient // Stream for DoPow
	requestID              uint64
	requestIDLock          *sync.Mutex
	outgoingReq            chan PendingPowRequest // Outgoing Requests to the powsrv.io
	pendingRequests        map[uint64]PendingPowRequest
	pendingRequestsLock    *sync.Mutex
	signalClosed           bool
	signalChanQuit         chan struct{}
}

func (client *PowClient) Init() error {
	var err error

	creds := credentials.NewTLS(&tls.Config{
		ServerName: serverName,
	})

	// Set up a connection to the server.
	client.connection, err = grpc.Dial(serverAddress, grpc.WithTransportCredentials(creds))
	if err != nil {
		return err
	}

	client.connectionErrorOcurred = make(chan struct{})

	client.signalClosed = false
	client.signalChanQuit = make(chan struct{})

	client.requestID = 0
	client.requestIDLock = &sync.Mutex{}

	client.outgoingReq = make(chan PendingPowRequest)

	client.pendingRequests = make(map[uint64]PendingPowRequest)
	client.pendingRequestsLock = &sync.Mutex{}

	md := metadata.Pairs("authorization", "powsrv-token "+client.APIKey)
	ctx := metadata.NewOutgoingContext(context.Background(), md)

	// Set up a RPC connection to the server.
	rpcClient := api.NewPowClient(client.connection)
	client.powStream, err = rpcClient.DoPow(ctx)
	if err != nil {
		return err
	}
	go client.handleConnection()

	return nil
}

func (client *PowClient) Close() {
	if !client.signalClosed {
		client.signalClosed = true

		close(client.signalChanQuit)
		close(client.outgoingReq)
		close(client.connectionErrorOcurred)

		client.powStream.CloseSend()
		client.connection.Close()
	}
}

func (client *PowClient) printMsg(msg string) {
	if client.Verbose {
		fmt.Print(msg + "\n")
	}
}

func (client *PowClient) handleConnection() {
	if client.connection == nil {
		panic("Connection not established")
	}
	if client.powStream == nil {
		panic("RPC Stream not established")
	}

	client.printMsg("Start receiving responses...")

	// Responses
	go func() {
		defer client.Close()

		for {
			incomingRes, err := client.powStream.Recv()

			if err != nil {
				_, ok := <-client.signalChanQuit
				if !ok {
					return
				}

				if err == io.EOF {
					client.printMsg("Connection to server closed")
				} else {
					client.printMsg(fmt.Sprintf("Failed to receive: %v", err))
				}

				client.connectionError = err
				close(client.connectionErrorOcurred)
				return
			}

			client.printMsg(fmt.Sprintf("Received response: %v", incomingRes))

			client.pendingRequestsLock.Lock()
			pendingPowRequest, ok := client.pendingRequests[incomingRes.Identifier]
			if ok {
				select {
				case <-client.signalChanQuit:
				case pendingPowRequest.powResponseChan <- incomingRes:
				}
			}
			client.pendingRequestsLock.Unlock()
		}
	}()

	// Requests
	for pendingPowRequest := range client.outgoingReq {
		client.pendingRequestsLock.Lock()
		client.pendingRequests[pendingPowRequest.powRequest.Identifier] = pendingPowRequest
		client.pendingRequestsLock.Unlock()

		err := client.powStream.Send(pendingPowRequest.powRequest)
		if err != nil {
			client.printMsg(fmt.Sprintf("Failed to send a message: %v", err))

			_, ok := <-client.signalChanQuit
			if !ok {
				return
			}

			pendingPowRequest.errorChan <- err
		}
	}
}

// PowFunc does the POW
func (client *PowClient) PowFunc(trytes trinary.Trytes, minWeightMagnitude int, parallelism ...int) (trinary.Trytes, error) {

	if client.connectionError != nil {
		return "", client.connectionError
	}

	if (minWeightMagnitude < 1) || (minWeightMagnitude > 14) {
		return "", fmt.Errorf("minWeightMagnitude out of range [1-14]: %v", minWeightMagnitude)
	}

	if client.connection == nil {
		return "", errors.New("Connection not established")
	}

	if client.powStream == nil {
		return "", errors.New("RPC Stream not established")
	}

	client.requestIDLock.Lock()
	client.requestID++
	reqID := client.requestID
	client.requestIDLock.Unlock()

	pendingPowRequest := PendingPowRequest{powRequest: &api.PowRequest{Identifier: reqID, Trytes: string(trytes), Mwm: uint32(minWeightMagnitude)}, powResponseChan: make(chan *api.PowResponse, 1), errorChan: make(chan error, 1)}
	defer close(pendingPowRequest.powResponseChan)
	defer close(pendingPowRequest.errorChan)

	// Send outgoing request
	select {
	case <-client.signalChanQuit:
		return "", errors.New("PowClient was closed")

	case client.outgoingReq <- pendingPowRequest:
	}

	defer func() {
		client.pendingRequestsLock.Lock()
		delete(client.pendingRequests, reqID)
		client.pendingRequestsLock.Unlock()
	}()

	// Wait for response
	select {
	case <-client.signalChanQuit:
		return "", errors.New("PowClient was closed")

	case powResponse, hasMore := <-pendingPowRequest.powResponseChan:
		if hasMore {
			// Got response
			result, err := trinary.NewTrytes(powResponse.Nonce)
			if err != nil {
				return "", err
			}

			return result, err
		}
		return "", errors.New("powResponseChan was closed")

	case err, hasMore := <-pendingPowRequest.errorChan:
		if hasMore {
			// Got error
			return "", err
		}
		return "", errors.New("errorChan was closed")

	case <-time.After(time.Duration(client.ReadTimeOutMs) * time.Millisecond):
		// Timeout

		client.printMsg(fmt.Sprintf("Receive timeout! ReqID: %v", reqID))

		return "", errors.New("Receive timeout")

	case <-client.connectionErrorOcurred:
		return "", client.connectionError
	}
}
